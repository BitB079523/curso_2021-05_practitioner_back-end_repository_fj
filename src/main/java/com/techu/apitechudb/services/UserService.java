package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll() {
        return this.userRepository.findAll();
    }

    public UserModel add(UserModel user) {
        System.out.println("add");

        return this.userRepository.save(user);
    }
    public Optional<UserModel> findById(String id) {
        System.out.println("findById");

        return this.userRepository.findById(id);
    }

    public UserModel update(UserModel user) {
        System.out.println("update");

        return this.userRepository.save(user);

    }

    public boolean delete(String id) {
        System.out.println("delete");

        boolean result = false;

        if (this.userRepository.findById(id).isPresent() == true) {
            System.out.println("He encontrado el Usuario a borrar:" + id + ", borrando...");

            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }

}
