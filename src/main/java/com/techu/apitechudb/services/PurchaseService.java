package com.techu.apitechudb.services;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import com.techu.apitechudb.repositories.PurchaseRepository;
import com.techu.apitechudb.models.PurchaseModel;

import java.util.List;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    public List<PurchaseModel> findAll() {
        System.out.println("findAll");

        return this.purchaseRepository.findAll();
    }
    public PurchaseModel add(PurchaseModel product) {
        System.out.println("add");

        return this.purchaseRepository.save(product);

    }
    public Optional<PurchaseModel> findById(String id) {
        System.out.println("findById");

        return this.purchaseRepository.findById(id);
    }

    public PurchaseModel update(PurchaseModel purchase) {
        System.out.println("update");

        return this.purchaseRepository.save(purchase);

    }
    public boolean delete(String id) {
        System.out.println("delete");

        boolean result = false;

        if (this.purchaseRepository.findById(id).isPresent() == true) {
            System.out.println("He encontrado el Producto a borrar:" + id +", borrando");

            this.purchaseRepository.deleteById(id);

            result = true;

        }
        return result;
    }
}
