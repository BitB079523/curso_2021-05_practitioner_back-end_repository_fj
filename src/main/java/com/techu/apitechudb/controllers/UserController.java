package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users/{id}")
    public RequestEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("getUserById");
        System.out.println("El Identificador (id) del usuario a buscar es: " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.getId() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/users/{id}")
    public RequestEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser");
        System.out.println("El Identificador (id) del usuario que se va a actualizar en parámetro URL es:" + id);
        System.out.println("El Identificador (id) del usuario que se va a actualizar es:" + user.getId());
        System.out.println("El nombre (name) del usuario que se va a actualizar es:" + user.getName());
        System.out.println("La edad (age) del usuario que se va a actualizar es:" + user.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById();

        if (userToUpdate.isPresent()) {
            System.out.println("Usuario para actualizar encontrado:" + id + ", actualizando...");

            this.userService.update(user);
        }

        return new ResponseEntity<>(
                user,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/users/{id}")
    public RequestEntity<UserModel> deleteUser(@PathVariable String id) {
            System.out.println("deleteUser");
            System.out.println("El Identificador (id) del usuario que se va a borrar en parámetro URL es: " + id);
            System.out.println("El Identificador (id) del usuario que se va a borrar es: " + user.getId());

            boolean deletedUser = this.userService.delete(id);

            return new ResponseEntity<>(
                    deletedUser ? "Usuario borrado" : "Usuario no encontrado",
                    deletedUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
            );

        }
}
