package com.techu.apitechudb.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;


@Document(collection = "purchases")
public class PurchaseModel {

    @Id
    private String id;
    private String userId;
    private float amount;
    private Map<String, Integer> purchaseItems;

    public String getId(String id) { return this.id = id;}

    public void setId(String id) { this.id = id;}

    public String getUserid() { return this.userid; }

    public void setUserid(String id) { this.id = userid;}

    public float getAmount() {return this.amount; }

    public void setAmount(float amount) { this.id = amount;}
}
