package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<UserModel, String> {
}
