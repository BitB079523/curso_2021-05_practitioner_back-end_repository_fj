package com.techu.apitechudb.repositories;


import com.techu.apitechudb.models.PurchaseModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PurchaseRepository extends MongoRepository<PurchaseModel, String> {

}
