package controllers;


import com.techu.apitechudb.controllers.HelloController;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

public class HelloControllerTest {

    @Test
    public void testHello() {
        HelloController sut = new HelloController();

        String test = sut.hello("Fco. Javier");

        //expected || actual
        Assert.assertEqual("Hola Fco. Javier!", sut.hello("Fco. Javier"))


    }
}
